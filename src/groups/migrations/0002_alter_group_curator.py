# Generated by Django 4.0.2 on 2022-03-10 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='curator',
            field=models.CharField(max_length=128),
        ),
    ]
