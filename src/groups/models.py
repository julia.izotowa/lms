import random
import uuid

from django.db import models
from faker import Faker


class Group(models.Model):
    id = models.UUIDField(
        primary_key=True, unique=True, editable=False, default=uuid.uuid4
    )
    group_name = models.CharField(max_length=128)
    curator = models.CharField(max_length=128, null=True)
    max_number_of_students = models.SmallIntegerField(default=0, null=True)
    budget_places_quota = models.SmallIntegerField(default=0, null=True)
    admission_year = models.DateField(null=True)
    graduation_year = models.DateField(null=True)

    def __str__(self):
        return f"{self.group_name}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                group_name=faker.word(),
                curator=faker.name(),
                max_number_of_students=random.randint(20, 30),
                budget_places_quota=random.randrange(50, 80, 5),
                admission_year=faker.date_between(start_date="-5y"),
                graduation_year=faker.date_between(start_date="today", end_date="+5y"),
            )
