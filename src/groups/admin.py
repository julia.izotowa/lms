from django.contrib import admin  # NOQA
from groups.models import Group


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('group_name',)
    fields = ('group_name',)
    actions_selection_counter = False
