from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from groups.models import Group


class GetGroupsView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:user_login")
    template_name = "groups-list.html"
    model = Group


class CreateGroupView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("students:user_login")
    template_name = "groups-create.html"
    http_method_names = ["get", "post"]
    model = Group
    fields = "__all__"
    success_url = reverse_lazy("groups:get_groups")


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("students:user_login")
    template_name = "groups-edit.html"
    http_method_names = ["get", "post"]
    model = Group
    fields = "__all__"
    success_url = reverse_lazy("groups:get_groups")


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("students:user_login")
    template_name = "groups_confirm_delete.html"
    http_method_names = ["get", "post"]
    model = Group
    success_url = reverse_lazy("groups:get_groups")
