from django.urls import path

from groups.views import (
    GetGroupsView,
    CreateGroupView,
    UpdateGroupView,
    DeleteGroupView,
)

app_name = "groups"

urlpatterns = [
    path("", GetGroupsView.as_view(), name="get_groups"),
    path("create/", CreateGroupView.as_view(), name="create_group"),
    path("update/<uuid:pk>/", UpdateGroupView.as_view(), name="update_group"),
    path("delete/<uuid:pk>/", DeleteGroupView.as_view(), name="delete_group"),
]
