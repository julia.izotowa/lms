import requests
import json


def cleanup_social_account(backend, uid, user=None, *args, **kwargs):
    print(kwargs)
    print(user)
    if backend.name == 'google-oauth2':
        user.photo = kwargs['response']['picture']
    elif backend.name == 'linkedin-oauth2':
        access_token = kwargs['response']['access_token']
        data = requests.get(
            f'https://api.linkedin.com/v2/me?projection=(profilePicture(displayImage~:playableStreams))'
            f'&oauth2_access_token={access_token}')
        data_dict = json.loads(data.text)
        user.photo = data_dict['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier']
    elif backend.name == 'github':
        user.photo = kwargs['response']['avatar_url']

    user.save()
    return {'user': user}
