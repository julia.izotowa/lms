from django.urls import path

from students.views import (
    GetStudentsView,
    CreateStudentView,
    UpdateStudentView,
    DeleteStudentView,
    SearchHistoryView,
    UserLoginView,
    UserLogoutView,
    UserRegistrationView,
    UserPasswordResetView,
    UserPasswordResetDoneView,
    UserPasswordResetConfirmView,
    UserPasswordResetCompleteView,
    ActivateUserView,
    UserProfile,
)

app_name = "students"

urlpatterns = [
    path("", GetStudentsView.as_view(), name="get_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:pk>/", DeleteStudentView.as_view(), name="delete_student"),
    path("history/", SearchHistoryView.as_view(), name="search_history"),
    path("user-login/", UserLoginView.as_view(), name="user_login"),
    path("user-logout/", UserLogoutView.as_view(), name="user_logout"),
    path("user-registration/", UserRegistrationView.as_view(), name="user_registration"),
    path("password_reset/", UserPasswordResetView.as_view(), name="password_reset"),
    path("password_reset/done/", UserPasswordResetDoneView.as_view(), name="password_reset_done", ),
    path("reset/<uidb64>/<token>/", UserPasswordResetConfirmView.as_view(), name="password_reset_confirm", ),
    path("reset/done/", UserPasswordResetCompleteView.as_view(), name="password_reset_complete", ),
    path("activate/<str:uuid64>/<str:token>/", ActivateUserView.as_view(), name="activate"),
    path("profile/<int:pk>/", UserProfile.as_view(), name="profile"),
]
