import datetime
import random
import uuid

from dateutil import relativedelta
from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinLengthValidator, FileExtensionValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from faker import Faker
from phonenumber_field.modelfields import PhoneNumberField

from config import settings
from students.managers import CustomUserManager
from students.validators import first_name_validator


class CustomUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_('name'), max_length=150, blank=True)
    last_name = models.CharField(_('surname'), max_length=150, blank=True)
    email = models.EmailField(_('email address'))
    phone_number = PhoneNumberField(_('phone number'), unique=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    photo = models.ImageField(upload_to='static/img/user_profile/')

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return str(self.phone_number)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_working_time(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class ProxyUser(get_user_model()):
    class Meta:
        proxy = True
        ordering = ('first_name',)

    def do_something(self):
        print(self.username)


class Profile(models.Model):
    USER_TYPE_CHOICES = [
        ("Student", "Student"),
        ("Teacher", "Teacher"),
        ("Mentor", "Mentor"),
    ]

    birthdate = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to=settings.STATIC_URL + 'img/user_profiles/')
    phone_number = models.CharField(max_length=16)
    sex = models.CharField(max_length=7)
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    user_type = models.CharField(choices=USER_TYPE_CHOICES, max_length=100)
    bio = models.TextField(default="")

    def __str__(self):
        return f"{self.user.id}_{self.user.username}_{self.user.last_name}"


@receiver(post_save, sender=get_user_model())
def create_profile_signal(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


class Person(models.Model):
    first_name = models.CharField(
        max_length=128,
        null=True,
        validators=[MinLengthValidator(2), first_name_validator],
    )
    last_name = models.CharField(
        max_length=128, null=True, validators=[MinLengthValidator(2)]
    )
    email = models.EmailField(max_length=128)
    birthdate = models.DateField(null=True)

    class Meta:
        abstract = True


class Student(Person):
    id = models.UUIDField(
        primary_key=True, unique=True, editable=False, default=uuid.uuid4
    )
    grade = models.SmallIntegerField(default=0, null=True)
    group = models.ForeignKey("groups.Group", null=True, on_delete=models.SET_NULL)
    avatar = models.ImageField(
        upload_to=settings.STATIC_URL + "img/avatars/", null=True
    )
    resume = models.FileField(
        upload_to=settings.STATIC_URL + "uploads/files/",
        null=True,
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
    )

    class Meta:
        verbose_name = "My student"
        verbose_name_plural = "All students"
        ordering = ['-first_name']

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def age(self):
        age = relativedelta.relativedelta(datetime.date.today(), self.birthdate)
        return age.years

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for i in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(start_date="-80y", end_date="-18y"),
                grade=random.randint(0, 100),
            )

    @classmethod
    def get_all_students(cls):
        return cls.objects.all()


class NewStudentInvitation(models.Model):
    id = models.UUIDField(
        primary_key=True, unique=True, editable=False, default=uuid.uuid4
    )
    inviter = models.ForeignKey(
        to="students.Student", null=True, on_delete=models.SET_NULL
    )
    invitation_email_address = models.EmailField(max_length=128)
    invitation_used = models.BooleanField(default=False)
