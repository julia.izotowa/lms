from datetime import date

from django.contrib import admin  # NOQA
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from students.models import Student, Profile


class DecadeBornListFilter(admin.SimpleListFilter):
    title = _('decade born')
    parameter_name = 'decade'

    def lookups(self, request, model_admin):
        return (
            ('other', _('other')),
            ('50s', _('in the fifties')),
            ('60s', _('in the sixties')),
            ('70s', _('in the seventies')),
            ('80s', _('in the eighties')),
            ('90s', _('in the nineties')),
            ('00s', _('in the aughts')),
            ('10s', _('in the tenth')),
            ('20s', _('in the twenties')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'other':
            return queryset.filter(birthdate__gte=date(2030, 1, 1),
                                   birthdate__lte=date(1950, 1, 1))
        if self.value() == '50s':
            return queryset.filter(birthdate__gte=date(1950, 1, 1),
                                   birthdate__lte=date(1959, 12, 31))
        if self.value() == '60s':
            return queryset.filter(birthdate__gte=date(1960, 1, 1),
                                   birthdate__lte=date(1969, 12, 31))
        if self.value() == '70s':
            return queryset.filter(birthdate__gte=date(1970, 1, 1),
                                   birthdate__lte=date(1979, 12, 31))
        if self.value() == '80s':
            return queryset.filter(birthdate__gte=date(1980, 1, 1),
                                   birthdate__lte=date(1989, 12, 31))
        if self.value() == '90s':
            return queryset.filter(birthdate__gte=date(1990, 1, 1),
                                   birthdate__lte=date(1999, 12, 31))
        if self.value() == '00s':
            return queryset.filter(birthdate__gte=date(2000, 1, 1),
                                   birthdate__lte=date(2009, 12, 31))
        if self.value() == '10s':
            return queryset.filter(birthdate__gte=date(2010, 1, 1),
                                   birthdate__lte=date(2019, 12, 31))
        if self.value() == '20s':
            return queryset.filter(birthdate__gte=date(2020, 1, 1),
                                   birthdate__lte=date(2029, 12, 31))


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    date_hierarchy = 'birthdate'
    ordering = ["email"]
    list_display = ('first_name', 'last_name', 'email', 'age', 'birthdate', 'group')
    # exclude = ('last_name',)
    list_display_links = ('first_name', 'last_name')
    search_fields = ('first_name__icontains', 'last_name')
    actions_selection_counter = True
    empty_value_display = '-'
    fields = ('first_name', 'last_name', 'email', 'birthdate', 'group')
    list_filter = (DecadeBornListFilter,)
    # radio_fields = {'group': admin.VERTICAL}
    save_as_continue = True
    sortable_by = ('first_name', 'last_name', 'group')


# admin.site.register(Student, StudentAdmin)
admin.site.register(Profile)
admin.site.register(get_user_model())
