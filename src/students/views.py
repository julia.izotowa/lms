import datetime

from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView,
)
from django.db.models import Q
from django.forms.utils import ErrorList
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import (
    TemplateView,
    ListView,
    CreateView,
    UpdateView,
    DeleteView, RedirectView, DetailView,
)

from students.forms import UserRegistrationForm
from students.models import Student, Profile
from students.services.emails import send_registration_email
from students.utils.token_generator import TokenGenerator


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]

    def get(self, request, *args, **kwargs):
        print(f"{self.request.current_time}")
        return super().get(request, *args, **kwargs)


class PageNotFoundView(TemplateView):
    template_name = "404.html"
    http_method_names = ["get"]


class GetStudentsView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:user_login")
    template_name = "students-list.html"
    model = Student

    def get_queryset(self):
        object_list = self.model.objects.all()
        self.request.session[
            f"search_{datetime.datetime.now()}"
        ] = self.request.GET.get("search_text")
        search_fields = ["first_name", "last_name", "email"]

        for param_name, param_value in self.request.GET.items():
            if param_value:
                if param_name == "search_text":
                    or_filter = Q()
                    for field in search_fields:
                        or_filter |= Q(**{f"{field}__icontains": param_value})
                    object_list = object_list.filter(or_filter)
                else:
                    object_list = object_list.filter(**{param_name: param_value})

        return object_list


class SearchHistoryView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:user_login")
    template_name = "search-history.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        history = []
        for key, value in self.request.session.items():
            if "search" in key:
                history.append((key, value))

        context["search_history_list"] = history
        return context

    def get_queryset(self):
        pass


class CreateStudentView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("students:user_login")
    template_name = "students-create.html"
    http_method_names = ["get", "post"]
    model = Student
    fields = ["first_name", "last_name", "email", "birthdate"]
    success_url = reverse_lazy("students:get_students")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        if first_name == last_name:
            form._errors["first_name"] = ErrorList(
                ["First name and last name can't be equal"]
            )
            return super().form_invalid(form)
        return super().form_valid(form)


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("students:user_login")
    template_name = "students-edit.html"
    http_method_names = ["get", "post"]
    model = Student
    fields = ["first_name", "last_name", "email", "birthdate"]
    success_url = reverse_lazy("students:get_students")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        if first_name == last_name:
            form._errors["first_name"] = ErrorList(
                ["First name and last name can't be equal"]
            )
            return super().form_invalid(form)
        return super().form_valid(form)


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("students:user_login")
    template_name = "students_confirm_delete.html"
    http_method_names = ["get", "post"]
    model = Student
    success_url = reverse_lazy("students:get_students")


class UserLoginView(LoginView):
    template_name = "registration/user_login.html"


class UserLogoutView(LogoutView):
    template_name = "registration/user_logout.html"


class UserRegistrationView(CreateView):
    template_name = "registration/registration.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(
            request=self.request,
            user_instance=self.object
        )
        return super().form_valid(form)


class UserPasswordResetView(PasswordResetView):
    success_url = reverse_lazy("students:password_reset_done")


class UserPasswordResetDoneView(PasswordResetDoneView):
    pass


class UserPasswordResetConfirmView(PasswordResetConfirmView):
    success_url = reverse_lazy("students:password_reset_complete")


class UserPasswordResetCompleteView(PasswordResetCompleteView):
    pass


class ActivateUserView(RedirectView):
    url = reverse_lazy('index')

    def get(self, request, uuid64, token, *args, **kwargs):
        user = get_user_model()
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = user.objects.get(pk=pk)
        except (user.DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user, backend='django.contrib.auth.backends.ModelBackend')
            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data")


class UserProfile(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy("students:user_login")
    template_name = 'profile.html'
    model = Profile
