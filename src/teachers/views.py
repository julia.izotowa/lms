from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms.utils import ErrorList
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from teachers.models import Teacher


class GetTeachersView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("students:user_login")
    template_name = "teachers-list.html"
    model = Teacher


class CreateTeacherView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("students:user_login")
    template_name = "teachers-create.html"
    http_method_names = ["get", "post"]
    model = Teacher
    fields = "__all__"
    success_url = reverse_lazy("teachers:get_teachers")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        if first_name == last_name:
            form._errors["first_name"] = ErrorList(
                ["First name and last name can't be equal"]
            )
            return super().form_invalid(form)
        return super().form_valid(form)


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("students:user_login")
    template_name = "teachers-edit.html"
    http_method_names = ["get", "post"]
    model = Teacher
    fields = "__all__"
    success_url = reverse_lazy("teachers:get_teachers")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        if first_name == last_name:
            form._errors["first_name"] = ErrorList(
                ["First name and last name can't be equal"]
            )
            return super().form_invalid(form)
        return super().form_valid(form)


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy("students:user_login")
    template_name = "teachers_confirm_delete.html"
    http_method_names = ["get", "post"]
    model = Teacher
    success_url = reverse_lazy("teachers:get_teachers")
