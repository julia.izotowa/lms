from django.contrib import admin  # NOQA
from django.utils.html import format_html

from teachers.models import Teacher


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    date_hierarchy = 'birthdate'
    list_display = ('first_name', 'last_name', 'email', 'birthdate', 'group_count', 'list_groups')
    actions_selection_counter = False
    # raw_id_fields = ('group',)
    # readonly_fields = ('list_groups',)

    fieldsets = (
        (
            "Personal info", {
                'fields': (('first_name', 'last_name'), 'academic_degree',)
            }
        ),
        (
            "Additional info", {
                'classes': ('collapse',),
                'fields': ('email', 'birthdate', 'group')
            }
        ),

    )

    @staticmethod
    def group_count(obj):
        if obj.group:
            return obj.group.all().count()
        else:
            return 0

    @staticmethod
    def list_groups(obj):
        if obj.group:
            groups = obj.group.all()
            # f"<a href='{reverse('admin:students_groups_group_change', args=group.pk)}'></a>"
            links = [
                f"<a href='http://127.0.0.1:8000/admin/groups/group/{group.pk}/change/'>{group.group_name}</a>"
                for group in groups
            ]
            return format_html(f"{'</br>'.join(links)}")
        return format_html("No groups")
# admin.site.register(Teacher, TeacherAdmin)
