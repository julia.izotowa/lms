import uuid

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from students.models import Person


class Teacher(Person):
    id = models.UUIDField(
        primary_key=True, unique=True, editable=False, default=uuid.uuid4
    )
    first_name = models.CharField(
        max_length=128, null=True, validators=[MinLengthValidator(2)]
    )
    specialization = models.CharField(max_length=128, null=True)
    academic_degree = models.CharField(max_length=256, null=True)
    group = models.ManyToManyField(to="groups.Group")

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_between(start_date="-80y", end_date="-18y"),
                specialization=faker.word(
                    ext_word_list=[
                        "Early Childhood Special Education",
                        "Applied Behavior Analysis",
                        "Visual and/or Hearing Impairment",
                        "English as a Second Language",
                        "Education Administration",
                    ]
                ),
                academic_degree=faker.suffix(),
            )
