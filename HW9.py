class Person:
    def __init__(self, name):
        self.name = name


class Group:
    def __init__(self, group_name, students):
        self.group_name = group_name
        self.students = students


class FastFoodEater:
    def __init__(self, name):
        self.name = name

    def type_of_food(self):
        print(f"{self.name} eat fastfood")


class HealthyFoodEater:
    def __init__(self, name):
        self.name = name

    def type_of_food(self):
        print(f"{self.name} eat healthy food")


class Teacher(Person, HealthyFoodEater):
    def __init__(self, name):
        super().__init__(name)

    def check_homework(self, student, mark):
        print(f'Teacher {self.name} gave a mark "{mark}" to the student {student.name}')

    def teach(self):
        pass


class Student(Person, FastFoodEater):
    def __init__(self, name, type_of_homework):
        super().__init__(name)
        self.type_of_homework = type_of_homework

    def do_homework(self):
        pass

    def hand_in_homework(self):
        self.type_of_homework.show()


class Presentation:
    def show(self):
        print("I show a presentation")


class Report:
    def show(self):
        print("I make a report")


if __name__ == "__main__":

    teacher_Mary = Teacher("Mary Wilkinson")
    group = Group(
        "MyGroup",
        [Student("Julia Izotowa", Report()), Student("Alex Johnson", Presentation())],
    )

    for student in group.students:
        student.hand_in_homework()
        teacher_Mary.check_homework(student, 100)

    for student in group.students:
        student.type_of_food()
    teacher_Mary.type_of_food()
